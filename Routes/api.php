<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/restservice', function (Request $request) {
    return $request->user();
});

Route::prefix('user')->group(function () {

    Route::post('/login', 'UserRestController@login')->name("rest-api.patient.login");

    Route::post('/register', 'UserRestController@register')->name("rest-api.patient.register");

    Route::get('/{patientId}', 'UserRestController@get_patient_details')->name("rest-api.patient.get-facilities");

    Route::get('/facilities', 'UserRestController@get_facilities')->name("rest-api.patient.get-facilities");

});

Route::prefix('patient')->group(function () {

    // medyq.localhost/api/patient/login

    Route::get('/test', 'PatientRestController@test')->name("the-rest-api.patient.test");

    Route::post('/login', 'PatientRestController@login')->name("the-rest-api.patient.login");

    Route::post('/register', 'PatientRestController@register')->name("the-rest-api.patient.register");

    Route::get('/{patientId}', 'PatientRestController@get_patient_details')->name("the-rest-api.patient.get-facilities");

    Route::get('/facilities', 'PatientRestController@get_facilities')->name("the-rest-api.patient.get-facilities");

});

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'PatientRestController@logout');
    Route::get('patient', 'PatientRestController@getAuthUser');
});