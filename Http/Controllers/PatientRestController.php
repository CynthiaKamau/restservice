<?php

namespace Modules\RestService\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Entities\Facility\Patient;
use App\Entities\Gender;
use App\Entities\MaritalStatus;
use App\Entities\Salutation;
use App\Entities\Facility;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Config;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class PatientRestController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    function __construct()
    {
        // Config::set('jwt.user', 'App\Facility\Patient');
        // Config::set('auth.providers.users.model', ['users' => [
        //     'patient' => 'eloquent',
        //     'model' => App\Facility\Patient::class
        // ]]);
    }
    
    public function register(Request $request, Facility $facility)
    {
        $validator = Validator::make($request->all(), [
            'salutation_id' => 'required|string',
            'first_name' => 'required|string|max:25',
            'middle_name' => 'nullable|string|max:25',
            'last_name' => 'required|string|max:25',
            'gender_id' => 'required|string',
            'marital_status_id' => 'required|string',
            'dob' => 'required',
            'residence' => 'required|string|max:255',
            //'phone_number'=>'required|string|unique:patients,phone_number,'.$id.',id|max:20',
            'email' => 'nullable|email|string|unique:patients,email',
            'is_dependant' => 'nullable|numeric|in:0,1',
            'id_type' => 'nullable|int|in:' . config('constants.id_types.passport') . ',' . config('constants.id_types.alien') . ',' . config('constants.id_types.id'),
            'id_number' => 'nullable|required_if:is_dependant,0|string|max:63',
            'status' => 'required|int|in:' . config('constants.status.alive') . ',' . config('constants.status.dead'),
            'password' => 'required|min:6',
            'number' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        // if ($patient->id === null) {

        //     $patient->number = generate_patient_number();

        // }

        $user = Patient::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password) ]
        ));

        return response()->json([
            'message' => 'Patient successfully registered',
            'user' => $user
        ], 201);

    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if(! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $patient = JWTAuth::authenticate($request->token);

        return response()->json(['patient', $patient]);
    }

    public function get_patient_details(Request $request, Facility $facility, Product $product)
    {
        $patient = array();

        $facilities = Facility::all();

        foreach($facilities as $facility) {

            $search_path = $facility['schema_name'];

            DB::statement("SET search_path = $search_path");

            $patient[] = Patient::with('maritalStatus')->get();

        }        

        return $patient;
    
    }

    public function get_facilities()
    {

        $patient = array();

        foreach($facilities as $facility) {

            $search_path = $facility['schema_name'];

            DB::statement("SET search_path = $search_path");

            $patient[] = Patient::with('maritalStatus')->get();

        }        

        return $patient;
    } 
    
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 'auth()->factory()->getTTL() * 60',
            'user' => auth()->user()
        ]);
    }
    
}
