<?php


namespace Modules\RestService\Http\Controllers;


use App\Entities\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


use Illuminate\Routing\Controller;

class UserRestController extends Controller
{
    use AuthenticatesUsers;

    public function register(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:190'],
            'last_name' => ['required', 'string', 'max:190'],
            'email' => 'required|string|email|max:190|unique:users,email',
            'password' => 'required|string|min:6|max:25',
            'status' => 'required',
            //'invite_code' => 'nullable|exists:users,referral_code',
            'agree' => 'required|integer|in:1',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if(! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }
   
    public function get_patient_details()
    {
        return User::with('groups.facility')->get();
    }
    
    public function get_facilities()
    {
        return User::with('groups.facility')->get();
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 'auth()->factory()->getTTL() * 60',
            'user' => auth()->user()
        ]);
    }

}